package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()
	r.GET("/favicon.ico", func(c *gin.Context) {
	    c.Status(http.StatusNoContent)
	})
	r.GET("get-ip", getIP)
	r.GET("healthcheck", healthCheck)
	r.Run(":8080")
}

// getIP ...
func getIP(c *gin.Context)  {
	clientIP := c.GetHeader("X-Real-Real-IP")
	if clientIP == "" {
		clientIP = c.GetHeader("X-Real-IP")
	}
	if clientIP == "" {
		clientIP = c.GetHeader("X-Forwarded-For")
	}
	if clientIP == "" {
		clientIP = c.ClientIP()
	}
	c.JSON(http.StatusOK, gin.H{
		"client_ip": clientIP,
	})
}

// healthCheck ...
func healthCheck(c *gin.Context)  {
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
	})
}
